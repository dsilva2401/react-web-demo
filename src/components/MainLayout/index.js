import React, { useState, useEffect, Fragment, createContext, useContext } from 'react';

const counterContext = createContext();

const CounterProvider = (props) => {
  const [counter, setCounter] = useState(0);
  return (
    <counterContext.Provider value={{
      counter: counter,
      setCounter: setCounter
    }}>
      { props.children }
    </counterContext.Provider>
  )
}

const ChildChildComponent = (props) => {
  const { counter } = useContext(counterContext);
  return (
    <div>Child Child component { counter }</div>
  )
}

const ChildComponent = (props) => {
  const { counter } = useContext(counterContext);
  return (
    <Fragment>
      <div>Child component { counter }</div>
      <ChildChildComponent />
    </Fragment>
  )
}

const Component = () => {
  const { counter } = useContext(counterContext);
  return (
    <Fragment>
      <div>Hello World! { counter }</div>
      <ChildComponent/>
    </Fragment>
  )
}

const MainComponent = () => {
  return (
    <CounterProvider>
      <Component />
    </CounterProvider>
  )
}


export default MainComponent;