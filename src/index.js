import React from 'react';
import ReactDOM from 'react-dom';

import MainLayout from './components/MainLayout';

ReactDOM.render(
  <div>
    <MainLayout />
  </div>,
  document.getElementById('root')
);
